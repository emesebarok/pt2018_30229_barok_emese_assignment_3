package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import DAO.OrderDao;
import model.Order;
import validators.OrderValidator;
import validators.Validator;

/*
 * Business Logic class for Orders
 * @author Barok Emese
 */

public class OrderBll {
	private List<Validator<Order>> validators;

	public OrderBll() {
		validators = new ArrayList<Validator<Order>>();
		validators.add(new OrderValidator());
	}

	/*
	 * Validates the order and returns the found order by id
	 */
	
	public Order findOrderById(int id) {
		Order st = OrderDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The order with id = " + id + " was not found!");
		}
		return st;
	}
	
	/*
	 * Validates the order and returns the found order by customer name.
	 */
	
	public Order findOrderByName(String name) {
		Order st = OrderDao.findByName(name);
		if (st == null) {
			throw new NoSuchElementException("The order with name = " + name + " was not found!");
		}
		return st;
	}
	/*
	 * Validates the orders and display them.
	 */
	
	public ArrayList<Order> displayOrders() {
		ArrayList<Order> st = OrderDao.displayOrder();
		if (st == null) {
			throw new NoSuchElementException("The order was not found!");
		}
		return st;
	}

	/*
	 * Validates the customer and instantiates the insert.
	 */
	public int insertOrder(Order order) {
		for (Validator<Order> v : validators) {
			v.validate(order);
		}
		return OrderDao.insert(order);
	}
}
