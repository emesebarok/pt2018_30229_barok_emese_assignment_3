package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.UserFrame;

/**
 * Controller for UserFrame.
 * Instantiates a UserFrame and defines button click behavior.
 * @author Barok Emese
 *
 */

public class UserController {
	UserFrame menu;
	/**
	 * Instantiates an UserController, sets the ActionsListeners for the two buttons on the AdminMenu frame.
	 * @param id ID of the non admin customer
	 */
	public UserController(final int id) {
		menu = new UserFrame();
			
			menu.getBtnModifyData().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ModifyDataController w = new ModifyDataController(id);
					menu.setVisible(false);
				}
			});
			menu.getBtnPlaceOrders().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					menu.setVisible(false);	
					PlaceOrderController d = new PlaceOrderController(id);		
				}
			});
		}
		
}
