package frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import bll.ProductsBll;
import model.Products;

/**
 * View which displays a list of products currently in the warehouse and gives the ability to the admin to modify them. New products can also be added.
 * @author Barok Emese
 *
 */

public class ModifyWareHouseFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btnAddProduct, btnBack, btnUpdate;
	private JTable t = null;
	String[]columns = {"IDProduct", "Name", "Price", "In Stock"};
	
	public ModifyWareHouseFrame() {
		this.setTitle("Modify Warehouse");
		this.setSize(600, 500);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		ProductsBll pb = new ProductsBll();
		ArrayList <Products> p = pb.displayProduct();
		int l = p.size();
		Object[][]dbi = new Object[l][4];
		for(int i = 0; i < l; i++) {
			int id = p.get(i).getProductid();
			String name = p.get(i).getName();
			int price = p.get(i).getPrice();
			int ins = p.get(i).getInStock();
			dbi[i] = new Object[] {id, name, price, ins};
		}
		
		TableModel model=new myTableModel(dbi);
		t =new JTable();
		t.setModel(model);
		t.setAutoCreateRowSorter(true);
		JScrollPane sp = new JScrollPane(t);
		sp.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		this.add(sp);
		
		
		JPanel south = new JPanel();
		south.setLayout(new GridLayout(1,3));
		
		btnBack = new JButton("Bact to menu");
		south.add(btnBack);
		
		btnAddProduct = new JButton("Add new product");
		south.add(btnAddProduct);
		
		btnUpdate = new JButton("Update");
		south.add(btnUpdate);
		
		this.add(south, BorderLayout.SOUTH);
		this.setVisible(true);
	}
	
	public JButton getBtnAddProduct() {
		return btnAddProduct;
	}
	
	public JButton getBtnBack() {
		return btnBack;
	}
	
	public JButton getBtnUpdate() {
		return btnUpdate;
	}
	
	public JTable getT() {
		return t;
	}
	
	public class myTableModel extends DefaultTableModel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		myTableModel(Object[][] dbi) {
			super(dbi,columns);
			}
		public boolean isCellEditable(int row,int cols) {
			if(cols==0 ){return false;}
			return true;
			}
	}
}


