package model;

import java.util.ArrayList;

/**
 * Model class representing an order.
 * @author Barok Emese
 *
 */

public class Order {
	private int orderid;
	private Customer customer;
	private ArrayList<Products> products;
	private int totalPrice;
	
	/**
	 * Creates new Order with the given id, customer, products, and total price.
	 * @param customerid ID of the order.
	 * @param customer Customer of the order.
	 * @param products Products of the order.
	 * @param totalPrice Total price of the order.
	 */
	
	public Order(int orderid, Customer customer, ArrayList<Products> products, int totalPrice) {
		this.orderid = orderid;
		this.customer = customer;
		this.products = products;
		this.totalPrice = totalPrice;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public ArrayList<Products> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<Products> products) {
		this.products = products;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public void addProduct(Products p) {
		products.add(p);
	}
}
