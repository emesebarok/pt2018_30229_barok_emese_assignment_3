package validators;

import model.Order;

/*
 * Implements the Validator<T> interface
 * @author Barok Emese
 */

public class OrderValidator implements Validator<Order> {

	/*
	 * Validates the price
	 * @see validators.Validator#validate(java.lang.Object)
	 */
	public void validate(Order t) {
		if (t.getTotalPrice() == 0) throw new IllegalArgumentException("TotalPrice is not a valid price!");
	}

}
