package frame;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class ModifyDataFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField name;
	private JTextField address;
	private JTextField phoneNumber;
	private JPasswordField password;
	private JButton btnModify;
	private JButton btnBack;
	
	public ModifyDataFrame() {
		this.setTitle("Modify Data");
		this.setSize(300, 300);
		this.setLayout(new GridLayout(0, 2));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.add(new JLabel("Name: "));
		name = new JTextField();
		this.add(name);
		
		this.add(new JLabel("Address: "));
		address = new JTextField();
		this.add(address);
		
		this.add(new JLabel("Phone Number: "));
		phoneNumber = new JTextField();
		this.add(phoneNumber);
		
		this.add(new JLabel("Password: "));
		password = new JPasswordField();
		this.add(password);
		
		btnModify = new JButton("Modify");
		btnBack = new JButton("Back");
		this.add(btnModify);
		this.add(btnBack);
		
		this.setVisible(true);
	}

	public String getName() {
		return name.getText();
	}
	
	public String getAddress() {
		return address.getText();
	}

	public int getPhoneNumber() {
		if (phoneNumber.getText().equals("")) return 0;
		return Integer.parseInt(phoneNumber.getText());
	}

	public JPasswordField getPassword() {
		return password;
	}
	
	public JButton getBtnModify() {
		return btnModify;
	}

	public JButton getBtnBack() {
		return btnBack;
	}
}
