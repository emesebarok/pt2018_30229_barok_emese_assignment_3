package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import bll.CustomerBll;
import frame.LoginFrame;
import model.Customer;

/**
 * Controller for LoginFrame. 
 * Authenticates user or admin.
 * @author Barok Emese
 *
 */

public class LoginController {
	public LoginFrame frame;
	
	/**
	 * Instantiates LoginFrame and waits for user input. If user name and password are correct a new frame is opened depending on the type of the user.
	 * For incorrect input an Error message is shown.
	 */
	
	public LoginController() {
		frame = new LoginFrame();
		
		/**
		 * Instantiates AdminMenuController if the user input is an admin, UserController otherwise.
		 */
		
		frame.getBtnLogin().addActionListener(new ActionListener() {

			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				try {
					CustomerBll cb = new CustomerBll();
					Customer c = cb.findCustomerByName(frame.getUsername());
					if (c.getPassword().equals(frame.getPassword().getText())) {
						if (c.isAdmin()) {
							AdminMenuController a = new AdminMenuController();
						}
						else {
							UserController u = new UserController(c.getCustomerid());
						}
						frame.setVisible(false);
					}
				}catch (Exception e1) {
					JOptionPane.showMessageDialog(frame, "Username or password is incorrect.", "Login Error",JOptionPane.ERROR_MESSAGE);
				}
				}
			
		});
		
		/**
		 * Instantiates NewCustomerController.
		 */
		
		frame.getBtnNewCustomer().addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				NewCustomerController n = new NewCustomerController();
				frame.setVisible(false);
			}
			
		});
	}
}
