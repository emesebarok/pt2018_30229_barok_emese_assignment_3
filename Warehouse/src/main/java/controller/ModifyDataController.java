package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import bll.CustomerBll;
import frame.ModifyDataFrame;
import model.Customer;

/**
 * Controller for ModifyDataFrame.
 * @author Barok Emese
 *
 */

public class ModifyDataController {
	private int id;
	private ModifyDataFrame frame;
	/**
	 * Instantiates an ModifyDataController, sets the ActionsListeners for the two buttons.
	 * @param id ID of the non admin customer
	 */
	public ModifyDataController(final int id) {
		this.id = id;
		frame = new ModifyDataFrame();
		
		frame.getBtnModify().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final Logger LOGGER = Logger.getLogger(NewCustomerController.class.getName());
				try {
					CustomerBll cb = new CustomerBll();
					Customer c = cb.findCustomerById(id);
					c = cb.findCustomerByName(c.getName());
					String name, address;
					int pn;
					name = frame.getName();
					if (!name.isEmpty()) c.setName(name);
					
					address = frame.getAddress();
					if (!address.isEmpty()) c.setAddress(address);
					
					pn = frame.getPhoneNumber();
					if(pn != 0) c.setPhoneNumber(pn);
					
					if (c.getPassword().equals(frame.getPassword().getText())) cb.updateCustomer(c);
					else JOptionPane.showMessageDialog(frame, "Password is incorrect.", "Modify Error",JOptionPane.ERROR_MESSAGE);
				}
				catch (Exception e1) {
					LOGGER.log(Level.INFO, e1.getMessage());
				}
			}
		});
		
		frame.getBtnBack().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				UserController l = new UserController(id);
				frame.setVisible(false);
			}
			
		});
	}
}
