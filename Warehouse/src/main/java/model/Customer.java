package model;

/**
 * Model class representing a customer.
 * @author Barok Emese
 *
 */

public class Customer {
	private int customerid;
	private String name;
	private String address;
	private int phoneNumber;
	private boolean isAdmin;
	private String password;
	
	/**
	 * Creates new Customer with the given id, name address, phone number and the boolean isAdmin.
	 * @param customerid ID of the customer.
	 * @param name Name of the customer.
	 * @param address Address of the customer.
	 * @param phoneNumber Phone number of the customer.
	 * @param isAdmin If the customer is admin it is true, otherwise is false.
	 */

	public Customer(int customerid, String name, String address, int phoneNumber, boolean isAdmin) {
		super();
		this.customerid = customerid;
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}
	
	/**
	 * Creates new Customer with the given id, name address, phone number, the boolean isAdmin and password.
	 * @param customerid ID of the customer.
	 * @param name Name of the customer.
	 * @param address Address of the customer.
	 * @param phoneNumber Phone number of the customer.
	 * @param isAdmin If the customer is admin it is true, otherwise is false.
	 * @param password Password of the customer.
	 */
	
	public Customer(int customerid, String name, String address, int phoneNumber, boolean isAdmin, String password) {
		super();
		this.customerid = customerid;
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.isAdmin = isAdmin;
		this.password = password;
	}
	
	/**
	 * Creates new Customer with the given name address, phone number, the boolean isAdmin and password.
	 * @param name Name of the customer.
	 * @param address Address of the customer.
	 * @param phoneNumber Phone number of the customer.
	 * @param isAdmin If the customer is admin it is true, otherwise is false.
	 * @param password Password of the customer.
	 */
	
	public Customer(String name, String address, int phoneNumber, boolean isAdmin, String password) {
		super();
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.isAdmin = isAdmin;
		this.password = password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public int getCustomerid() {
		return customerid;
	}

	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
