package frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import bll.OrderBll;
import model.Order;

/**
 * Extension of the Swing JFrame class which contains information about the product.
 * @author Barok Emese
 *
 */

public class DisplayOrdersFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btnBack = new JButton("Back");
	
	String[]columns = {"name", "quantity", "price"};

	/**
	 * Creates the frame with the help of several Java Swing classes and an instance of the Order class which is the source of information.
	 */
	
	public DisplayOrdersFrame() {
		this.setTitle("Display Orders");
		this.setSize(600, 500);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		JPanel p;
		JPanel op = new JPanel();
		op.setLayout(new GridLayout(0, 2));
		op.setPreferredSize(new Dimension(480, 80));
		op.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		
		OrderBll ob = new OrderBll();
		ArrayList <Order> o = ob.displayOrders();
		
		JTable table = null;
		
		int l = o.size();
		for (int i = 0; i < l; i++) {
			
			p = new JPanel();
			p.setLayout(new GridLayout(4,1));
			p.add(new JLabel("Order ID: " + o.get(i).getOrderid()));
			p.add(new JLabel("Customer name: " + o.get(i).getCustomer().getName()));
			p.add(new JLabel("Customer address: " + o.get(i).getCustomer().getAddress()));
			p.add(new JLabel("Total price: " + o.get(i).getTotalPrice()));
			p.setBorder(BorderFactory.createLineBorder(Color.black, 2));
			op.add(p);
			
			String name;
			int quantity, price;
			int s = o.get(i).getProducts().size();
			Object[][]dbi = new Object[s][3];
			for (int j = 0; j < s; j++) {
				name = o.get(i).getProducts().get(j).getName();
				quantity = o.get(i).getProducts().get(j).getQuantity();
				price = o.get(i).getProducts().get(j).getPrice();
				dbi[j] = new Object[] {name, quantity, price};
			}
			
			table = new JTable(dbi, columns);
			table.setAutoCreateRowSorter(true);
			table.setEnabled(false);
			JScrollPane sp = new JScrollPane(table);
			sp.setBorder(BorderFactory.createLineBorder(Color.black, 2));
			op.add(sp);
		}
	    
	    JScrollPane scrollPane = new JScrollPane(op);
        this.add(scrollPane, BorderLayout.CENTER);
        btnBack.setSize(600, 15);
        this.add(btnBack, BorderLayout.SOUTH);
        this.setVisible(true);


	}

	public JButton getBtnBack() {
		return btnBack;
	}
	
}
