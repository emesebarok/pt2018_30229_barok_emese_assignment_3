package frame;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * Extension of the Swing JFrame class which authenticates user or admin.
 * @author Barok Emese
 *
 */

public class LoginFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField username;
	private JPasswordField password;
	private JButton btnLogin;
	private JButton btnNewCustomer;
	
	/**
	 * Creates the frame with the help of several Java Swing classes.
	 */
	
	public LoginFrame() {
		this.setTitle("Login");
		this.setSize(300, 300);
		this.setLayout(new GridLayout(0,1));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.add(new JLabel("Please enter your credentials: "));
		
		JPanel uName = new JPanel(new GridLayout(1,2));
		uName.add(new JLabel("Username: "));
		username = new JTextField();
		uName.add(username);
		this.add(uName);
		
		JPanel pass = new JPanel(new GridLayout(1,2));
		pass.add(new JLabel("Password: "));
		password = new JPasswordField();
		pass.add(password);
		this.add(pass);
		
		btnLogin = new JButton("Login");
		btnNewCustomer = new JButton("New Customer");
		this.add(btnNewCustomer);
		this.add(btnLogin);
		
		this.setVisible(true);
	}

	public String getUsername() {
		return username.getText();
	}

	public JPasswordField getPassword() {
		return password;
	}

	public JButton getBtnLogin() {
		return btnLogin;
	}

	public JButton getBtnNewCustomer() {
		return btnNewCustomer;
	}

}
