package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.AdminMenuFrame;

/**
 * Controller containing the code which reacts to user input on a given AdminMenu frame.
 * @author Barok Emese
 *
 */
public class AdminMenuController {
	AdminMenuFrame menu;
	
	/**
	 * Instantiates an AdminMenuController.
	 */
	public AdminMenuController() {
		menu = new AdminMenuFrame();
		
		menu.getBtnDisplayOrders().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ModifyWareHouseController w = new ModifyWareHouseController();
				menu.setVisible(false);
			}
			
		});
		
		menu.getBtnModifyWarehouse().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				menu.setVisible(false);	
				DisplayOrderController d = new DisplayOrderController();		
			}
		});
	}
}
