package frame;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class NewProductFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField name;
	private JTextField price;
	private JTextField inStock;
	private JButton btnAdd;
	private JButton btnBack;
	
	public NewProductFrame() {
		this.setTitle("New Product");
		this.setSize(300, 300);
		this.setLayout(new GridLayout(0, 2));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.add(new JLabel("Name: "));
		name = new JTextField();
		this.add(name);
		
		this.add(new JLabel("Price: "));
		price = new JTextField();
		this.add(price);
		
		this.add(new JLabel("In Stock: "));
		inStock = new JTextField();
		this.add(inStock);
		
		btnAdd = new JButton("Add");
		btnBack = new JButton("Back");
		this.add(btnAdd);
		this.add(btnBack);
		
		this.setVisible(true);
	}

	public String getName() {
		return name.getText();
	}

	public int getPrice() {
		return Integer.parseInt(price.getText());
	}
	
	public int getInStock() {
		return Integer.parseInt(inStock.getText());
	}

	public JButton getBtnAdd() {
		return btnAdd;
	}

	public JButton getBtnBack() {
		return btnBack;
	}
}
