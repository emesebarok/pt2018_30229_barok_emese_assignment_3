package frame;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class NewCustomerFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField name;
	private JTextField address;
	private JTextField phoneNumber;
	private JTextField password;
	private JButton btnAdd;
	private JButton btnBack;
	
	public NewCustomerFrame() {
		this.setTitle("New Customer");
		this.setSize(300, 300);
		this.setLayout(new GridLayout(0, 2));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.add(new JLabel("Name: "));
		name = new JTextField();
		this.add(name);
		
		this.add(new JLabel("Address: "));
		address = new JTextField();
		this.add(address);
		
		this.add(new JLabel("Phone Number: "));
		phoneNumber = new JTextField();
		this.add(phoneNumber);
		
		this.add(new JLabel("Password: "));
		password = new JTextField();
		this.add(password);
		
		btnAdd = new JButton("Add");
		btnBack = new JButton("Back");
		this.add(btnAdd);
		this.add(btnBack);
		
		this.setVisible(true);
	}

	public String getName() {
		return name.getText();
	}
	
	public String getAddress() {
		return address.getText();
	}

	public int getPhoneNumber() {
		return Integer.parseInt(phoneNumber.getText());
	}

	public String getPassword() {
		return password.getText();
	}

	public JButton getBtnAdd() {
		return btnAdd;
	}

	public JButton getBtnBack() {
		return btnBack;
	}
}
