package validators;

import model.Customer;

/*
 * Implements the Validator<T> interface
 * @author Barok Emese
 */

public class PhoneNumberValidator implements Validator<Customer> {
	/*
	 * Validates the phone number of the customer. 
	 * @see validators.Validator#validate(java.lang.Object)
	 */
	public void validate(Customer t) {
		if (t.getPhoneNumber() == 0) {
			throw new IllegalArgumentException("PhoneNumber is not a valid phone number!");
		}
	}
}
