package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.ProductsBll;
import frame.NewProductFrame;
import model.Products;

/**
 * Controller for NewProductFrame.
 * @author Barok Emese
 *
 */

public class NewProductController {
	private NewProductFrame frame;
	public NewProductController() {
		frame = new NewProductFrame();
		
		/**
		 * Extension of the ActionListener Class which describes the behavior of the Add Product button
		 * Adds new product to the database.
		 * @author Barok Emese
		 *
		 */
		
		frame.getBtnAdd().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				final Logger LOGGER = Logger.getLogger(NewCustomerController.class.getName());
				try {
					ProductsBll pb = new ProductsBll();
					int i = pb.displayProduct().size();
					pb.insertProducts(new Products(i + 1, frame.getName(), frame.getPrice(), frame.getInStock()));
				}
				catch (Exception e1) {
					LOGGER.log(Level.INFO, e1.getMessage());
				}
				frame.setVisible(false);
			}
			
		});
		
		/**
		 * Instantiates ModifyWareHouseController.
		 */
		
		frame.getBtnBack().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				ModifyWareHouseController l = new ModifyWareHouseController();
				frame.setVisible(false);
			}
			
		});
	}
}
