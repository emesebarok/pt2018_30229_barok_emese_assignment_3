package frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import bll.ProductsBll;
import model.Products;

@SuppressWarnings("serial")
public class PlaceOrderFrame extends JFrame {
	
	private JButton btnBack, btnUpdateCart, btnFinalizeOrder;
	private JTable t = null;
	private JLabel price = new JLabel("Total Price: 0");
	private JTextField tf = new JTextField();
	
	String[]columns = {"IDProduct", "Name", "Price", "In Stock", "Quantity"};
	
	public PlaceOrderFrame() {
		this.setTitle("Place Order");
		this.setSize(600, 500);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		ProductsBll pb = new ProductsBll();
		ArrayList <Products> p = pb.displayProduct();
		int l = p.size();
		Object[][]dbi = new Object[l][5];
		JPanel order = new JPanel();
		order.setLayout(new GridLayout(1, 2));
		int i = 0;
		for(Products s : p) {
			int id = s.getProductid();
			String name = s.getName();
			int price = s.getPrice();
			int ins = s.getInStock();
			dbi[i] = new Object[] {id, name, price, ins, "0"};
			i++;
		}
		
		TableModel model = new myTableModel(dbi);
		t = new JTable();
		t.setModel(model);
		t.setAutoCreateRowSorter(true);
		JScrollPane sp = new JScrollPane(t);
		sp.setSize(300, t.getRowHeight());
		sp.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		
		JPanel cart = new JPanel();
		cart.setLayout(new BorderLayout());
		cart.add(price, BorderLayout.PAGE_START);
		tf.setEditable(false);
		tf.setSize(100, 100);
		cart.add(tf);
		
		JPanel big = new JPanel();
		big.setLayout(new GridLayout(2, 1));
		big.add(sp);
		big.add(cart);
		
		
		JPanel south = new JPanel();
		south.setLayout(new GridLayout(1,3));
		
		btnBack = new JButton("Bact to menu");
		south.add(btnBack);
		
		btnUpdateCart = new JButton("Update Cart");
		south.add(btnUpdateCart);
		
		btnFinalizeOrder = new JButton("Finalize Order");
		south.add(btnFinalizeOrder);
		
		this.add(big);
		this.add(south, BorderLayout.PAGE_END);
		//this.pack();

		this.setVisible(true);
	}

	public JButton getBtnBack() {
		return btnBack;
	}
	
	public JButton getBtnUpdate() {
		return btnUpdateCart;
	}
	
	public JButton getBtnFinalizeOrder() {
		return btnFinalizeOrder;
	}
	
	public JTable getTable() {
		return t;
	}
	
	public JTextField getTextField() {
		return tf;
	}
	
	public JLabel getLabel() {
		return price;
	}
	
	public class myTableModel extends DefaultTableModel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		myTableModel(Object[][] dbi) {
			super(dbi,columns);
			}
		public boolean isCellEditable(int row,int cols) {
			if(cols == 4){return true;}
			return false;
			}
	}
}
