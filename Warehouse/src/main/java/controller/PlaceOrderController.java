package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import bll.*;
import frame.PlaceOrderFrame;
import model.*;

public class PlaceOrderController {
	PlaceOrderFrame frame;
	ArrayList<Products> ap = new ArrayList<Products>();
	int p = 0;
	
	/**
	 * Instantiates an PlaceOrderController, sets the ActionsListeners for the three buttons.
	 * @param id ID of the non admin customer
	 */
	
	public PlaceOrderController(final int id) {
		frame = new PlaceOrderFrame();
		
		frame.getBtnBack().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				UserController l = new UserController(id);
				frame.setVisible(false);
			}
			
		});
		
		frame.getBtnUpdate().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String cart = "", s;
				int l = frame.getTable().getRowCount();
				for (int i = 0; i < l; i++) {
					s = frame.getTable().getValueAt(i, 4).toString();
					if(!s.equals("0")) {
						int inStock = Integer.parseInt(frame.getTable().getValueAt(i, 3).toString());
						if (inStock < Integer.parseInt(s)) {
							JOptionPane.showMessageDialog(frame, "Sorry, out of this product.", "Update Cart Error",JOptionPane.ERROR_MESSAGE);
						}
						else {
							String name = frame.getTable().getValueAt(i, 1).toString();
							int price = Integer.parseInt(frame.getTable().getValueAt(i, 2).toString());
							int quantity = Integer.parseInt(frame.getTable().getValueAt(i, 4).toString());
							cart += name + " * " + s + "\n";
							p += price * quantity;
							int productid = Integer.parseInt(frame.getTable().getValueAt(i, 0).toString());
							ap.add(new Products(productid, name, price, quantity, inStock));
						}
					}
				}
				frame.getTextField().setText(cart);
				frame.getLabel().setText("Total price: " + p);
			}
			
		});
		
		frame.getBtnFinalizeOrder().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				try {
					OrderBll ob = new OrderBll();
					CustomerBll cb = new CustomerBll();
					ProductsBll pb = new ProductsBll();
					ArrayList<Order> ao = new ArrayList<Order>();
					ao = ob.displayOrders();
					int newOrderIndex = ao.get(ao.size() - 1).getOrderid() + 1; //the next orderid will be the last orderid + 1
					Customer c = cb.findCustomerById(id);
					ob.insertOrder(new Order(newOrderIndex, c, ap, p));
					for (Products f : ap) {
						f.setInStock(f.getInStock() - f.getQuantity());
						pb.updateStocks(f);
					}
					JOptionPane.showMessageDialog(frame, "Thank you for ordering from us.");
				}catch (Exception e1) {
					JOptionPane.showMessageDialog(frame, "Something went wrong. Try again later.");
				}
			}
			
		});
	}
}
