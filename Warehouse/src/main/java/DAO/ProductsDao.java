package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Products;

/*
 * Data access class for products
 * @author Barok Emese
 */

public class ProductsDao {
	protected static final Logger LOGGER = Logger.getLogger(CustomersDao.class.getName());
	private static final String insertStatementString = "INSERT INTO products (idproducts, name, price, inStock)" + " VALUES (?,?,?,?)";
	private final static String findStatementString1 = "SELECT * FROM products where idproducts = ?";
	private final static String findStatementString2 = "SELECT * FROM products";
	private final static String updateStatementString = "UPDATE products SET `name` = ?, `price` = ?, `inStock` = ? WHERE `idproducts` = ?";
	private final static String updateStockString = "UPDATE products SET `inStock` = ? WHERE `idproducts` = ?";

	/*
	 * Instantiates the connection to the database, prepare and execute the query, then returns the product found by id
	 */
	
	public static Products findById(int productid) {
		Products toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString1);
			findStatement.setLong(1, productid);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			int price = rs.getInt("price");
			int inStock = rs.getInt("inStock");
			toReturn = new Products(productid, name, price, inStock);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductsDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then returns the found by products
	 */
	
	public static ArrayList<Products> displayProducts() {
		ArrayList<Products> toReturn = new ArrayList<Products>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString2);
			rs = findStatement.executeQuery();

			while (rs.next()) {
				String name = rs.getString("name");
				int price = rs.getInt("price");
				int inStock = rs.getInt("inStock");
				toReturn.add(new Products(rs.getInt("idproducts"), name, price, inStock));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductsDAO:displayProducts" + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then inserts the product
	 */

	public static int insert(Products product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, product.getProductid());
			insertStatement.setString(2, product.getName());
			insertStatement.setInt(3, product.getPrice());
			insertStatement.setInt(4, product.getInStock());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductsDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then updates the product
	 */
	
	public static int update(Products product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		int updatedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, product.getName());
			updateStatement.setInt(2, product.getPrice());
			updateStatement.setInt(3, product.getInStock());
			updateStatement.setInt(4, product.getProductid());
			updateStatement.executeUpdate();

			ResultSet rs = updateStatement.getGeneratedKeys();
			if (rs.next()) {
				updatedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductsDao:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return updatedId;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then updates the product's stock
	 */
	
	public static int updateStock(Products product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStockStatement = null;
		int updatedStockId = -1;
		try {
			updateStockStatement = dbConnection.prepareStatement(updateStockString, Statement.RETURN_GENERATED_KEYS);
			updateStockStatement.setInt(1, product.getInStock());
			updateStockStatement.setInt(2, product.getProductid());
			updateStockStatement.executeUpdate();

			ResultSet rs = updateStockStatement.getGeneratedKeys();
			if (rs.next()) {
				updatedStockId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductsDao:updateStock " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStockStatement);
			ConnectionFactory.close(dbConnection);
		}
		return updatedStockId;
	}
}
