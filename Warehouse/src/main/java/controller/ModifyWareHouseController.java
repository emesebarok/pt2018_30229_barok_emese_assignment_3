package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import bll.ProductsBll;
import frame.ModifyWareHouseFrame;
import model.Products;

/**
 * Controller for ModifyWareHouseFrame.
 * @author Barok Emese
 *
 */

public class ModifyWareHouseController {
	ModifyWareHouseFrame frame;
	public ModifyWareHouseController() {
		frame = new ModifyWareHouseFrame();
		
		/**
		 * Instantiates an AdminMenuController.
		 */
		
		frame.getBtnBack().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				AdminMenuController a = new AdminMenuController();
				frame.setVisible(false);
			}
			
		});
		
		/**
		 * Updates the database's products table.
		 */
		
		frame.getBtnUpdate().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				ProductsBll pb = new ProductsBll();
				int l = frame.getT().getModel().getRowCount();
				int id, price, inStock;
				String name;
				for (int i = 0; i < l; i++) {
					id = Integer.parseInt(frame.getT().getModel().getValueAt(i, 0).toString());
					name = frame.getT().getModel().getValueAt(i, 1).toString();
					price = Integer.parseInt(frame.getT().getModel().getValueAt(i, 2).toString());
					inStock = Integer.parseInt(frame.getT().getModel().getValueAt(i, 3).toString());
					pb.updateProducts(new Products(id, name, price, inStock));
				}
			}
			
		});
		
		/**
		 * Instantiates an NewProductController.
		 */
		
		frame.getBtnAddProduct().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				NewProductController p = new NewProductController();
				frame.setVisible(false);
			}
			
		});
	}
}
