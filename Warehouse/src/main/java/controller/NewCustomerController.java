package controller;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.CustomerBll;
import frame.NewCustomerFrame;
import model.Customer;

/**
 * Controller for NewCustomerFrame.
 * @author Barok Emese
 *
 */

public class NewCustomerController {
	private NewCustomerFrame frame;
	public NewCustomerController() {
		frame = new NewCustomerFrame();
		
		/**
		 * Extension of the ActionListener Class which describes the behavior of the Add Customer button
		 * Adds new customer to the database.
		 * @author Barok Emese
		 *
		 */
		
		frame.getBtnAdd().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				final Logger LOGGER = Logger.getLogger(NewCustomerController.class.getName());
				try {
					CustomerBll cb = new CustomerBll();
					cb.insertCustomer(new Customer(frame.getName(), frame.getAddress(), frame.getPhoneNumber(), false, frame.getPassword()));
				}
				catch (Exception e1) {
					LOGGER.log(Level.INFO, e1.getMessage());
				}
				frame.setVisible(false);
			}
			
		});
		
		/**
		 * Instantiates LoginController.
		 */
		
		frame.getBtnBack().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				LoginController l = new LoginController();
				frame.setVisible(false);
			}
			
		});
	}
}
