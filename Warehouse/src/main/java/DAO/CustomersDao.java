package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Customer;

/*
 * Data access class for customers
 * @author Barok Emese
 */

public class CustomersDao {
	protected static final Logger LOGGER = Logger.getLogger(CustomersDao.class.getName());
	private static final String insertStatementString = "INSERT INTO customer (name, address, phoneNumber, isAdmin, password)" + " VALUES (?,?,?,?,?)";
	private static final String updateStatementString = "UPDATE customer SET `name` = ?, `address` = ?, `phoneNumber` = ?, `password` = ? WHERE `idcustomer` = ?";
	private final static String findStatementString1 = "SELECT * FROM customer where idcustomer = ?";
	private final static String findStatementString2 = "SELECT * FROM customer where name = ?";

	/*
	 * Instantiates the connection to the database, prepare and execute the query, then returns the customer found by id
	 */
	
	public static Customer findById(int customerid) {
		Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString1);
			findStatement.setLong(1, customerid);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			String address = rs.getString("address");
			int phoneNumber = rs.getInt("phoneNumber");
			toReturn = new Customer(customerid, name, address, phoneNumber, rs.getBoolean("isAdmin"));
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomersDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then returns the customer found by name
	 */
	
	public static Customer findByName(String name) {
		Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString2);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();

			int customerid = rs.getInt("idcustomer");
			String address = rs.getString("address");
			int phoneNumber = rs.getInt("phoneNumber");
			boolean a;
			if (rs.getInt("isAdmin") == 1) a = true;
			else a = false;
			//System.out.println(rs.getInt("isAdmin"));
			toReturn = new Customer(customerid, name, address, phoneNumber, a, rs.getString("password"));
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomersDAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then inserts the customer
	 */

	public static int insert(Customer customer) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, customer.getName());
			insertStatement.setString(2, customer.getAddress());
			insertStatement.setInt(3, customer.getPhoneNumber());
			insertStatement.setBoolean(4, customer.isAdmin());
			insertStatement.setString(5, customer.getPassword());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomersDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then updates the customer
	 */
	
	public static int update(Customer customer) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		int updatedId = -1;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, customer.getName());
			updateStatement.setString(2, customer.getAddress());
			updateStatement.setInt(3, customer.getPhoneNumber());
			updateStatement.setString(4, customer.getPassword());
			updateStatement.setInt(5, customer.getCustomerid());
			updateStatement.executeUpdate();

			ResultSet rs = updateStatement.getGeneratedKeys();
			if (rs.next()) {
				updatedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomersDao:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return updatedId;
	}
}
