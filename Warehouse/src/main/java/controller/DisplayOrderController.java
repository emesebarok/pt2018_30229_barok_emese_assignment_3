package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import frame.DisplayOrdersFrame;

/**
 * Controller containing the code which reacts to the Display Order button from the AdminMenu frame.
 * @author Barok Emese
 *
 */

public class DisplayOrderController {
	 DisplayOrdersFrame frame;
	 
	 public DisplayOrderController() {
		 frame = new DisplayOrdersFrame();
		 
		 /**
		* If the user clicks the Back Button the current frame will be disposed of and the AdminMenuController will be instantiated.
		* @author Barok Emese
		*
		*/
		 frame.getBtnBack().addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent arg0) {
					AdminMenuController a = new AdminMenuController();
					frame.setVisible(false);
				}
				
			});
	 }
	 
	
}
