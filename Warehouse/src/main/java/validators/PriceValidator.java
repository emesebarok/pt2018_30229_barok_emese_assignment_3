package validators;

import model.Products;

/*
 * Implements the Validator<T> interface
 * @author Barok Emese
 */

public class PriceValidator implements Validator<Products> {
	/*
	 * Validates the price of a product.
	 * @see validators.Validator#validate(java.lang.Object)
	 */
	public void validate(Products t) {
		if (t.getPrice() < 0) {
			throw new IllegalArgumentException("PhoneNumber is not a valid phone number!");
		}
	}
}
