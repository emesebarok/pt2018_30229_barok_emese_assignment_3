package model;

/**
 * Model class representing a product.
 * @author Barok Emese
 *
 */

public class Products {
	private int productid;
	private String name;
	private int price;
	private int inStock;
	private int quantity = 0;
	
	/**
	 * Creates new Product with the given id, name, price, and inStock.
	 * @param productid ID of the product.
	 * @param name Name of the product.
	 * @param price Price of the product.
	 * @param inStock Number of the product in stock.
	 */
	
	public Products(int productid, String name, int price, int inStock) {
		this.productid = productid;
		this.name = name;
		this.price = price;
		this.inStock = inStock;
	}
	
	/**
	 * Creates new Product with the given id, name, price, quantity, and inStock.
	 * @param productid ID of the product.
	 * @param name Name of the product.
	 * @param price Price of the product.
	 * @param quantity Quantity of the product.
	 * @param inStock Number of the product in stock.
	 */
	
	public Products(int productid, String name, int price, int quantity, int inStock) {
		this.productid = productid;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.inStock = inStock;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getInStock() {
		return inStock;
	}

	public void setInStock(int inStock) {
		this.inStock = inStock;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
