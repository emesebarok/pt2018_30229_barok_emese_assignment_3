package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import DAO.ProductsDao;
import model.Products;
import validators.PriceValidator;
import validators.Validator;

/*
 * Business Logic class for Products
 * @author Barok Emese
 */

public class ProductsBll {
	private List<Validator<Products>> validators;

	public ProductsBll() {
		validators = new ArrayList<Validator<Products>>();
		validators.add(new PriceValidator());
	}
	
	/*
	 * Validates the product and returns the found product by id
	 */

	public Products findById(int id) {
		Products st = ProductsDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return st;
	}
	
	/*
	 * Validates the products and display them.
	 */
	
	public ArrayList<Products> displayProduct() {
		ArrayList<Products> st = ProductsDao.displayProducts();
		if (st == null) {
			throw new NoSuchElementException("The products were not found!");
		}
		return st;
	}

	/*
	 * Validates the product and instantiates the insert.
	 */
	
	public int insertProducts(Products product) {
		for (Validator<Products> v : validators) {
			v.validate(product);
		}
		return ProductsDao.insert(product);
	}
	
	/*
	 * Validates the customer and instantiates the product update
	 */
	
	public int updateProducts(Products product) {
		for (Validator<Products> v : validators) {
			v.validate(product);
		}
		return ProductsDao.update(product);
	}
	
	/*
	 * Validates the customer and instantiates the product's stock update
	 */
	
	public int updateStocks(Products product) {
		for (Validator<Products> v : validators) {
			v.validate(product);
		}
		return ProductsDao.updateStock(product);
	}
}
