package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import DAO.CustomersDao;
import model.Customer;
import validators.*;

/*
 * Business Logic class for Customers
 * @author Barok Emese
 */

public class CustomerBll {
	private List<Validator<Customer>> validators;

	public CustomerBll() {
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new PhoneNumberValidator());
	}

	/*
	 * Validates the customer and returns the found customer by id
	 */
	public Customer findCustomerById(int id) {
		Customer st = CustomersDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The customer with id =" + id + " was not found!");
		}
		return st;
	}
	
	/*
	 * Validates the customer and returns the found customer by name
	 */
	
	public Customer findCustomerByName(String name) {
		Customer st = CustomersDao.findByName(name);
		if (st == null) {
			throw new NoSuchElementException("The customer with name =" + name + " was not found!");
		}
		return st;
	}

	/*
	 * Validates the customer and instantiates the insert
	 */
	public int insertCustomer(Customer customer) {
		for (Validator<Customer> v : validators) {
			v.validate(customer);
		}
		return CustomersDao.insert(customer);
	}
	
	/*
	 * Validates the customer and instantiates the update
	 */
	
	public int updateCustomer(Customer customer) {
		for (Validator<Customer> v : validators) {
			v.validate(customer);
		}
		return CustomersDao.update(customer);
	}
}
