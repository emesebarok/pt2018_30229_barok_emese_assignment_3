package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


import connection.ConnectionFactory;
import model.Customer;
import model.Order;
import model.Products;

/*
 * Data access class for orders
 * @author Barok Emese
 */

public class OrderDao {
	protected static final Logger LOGGER = Logger.getLogger(OrderDao.class.getName());
	private static final String insertStatementString1 = "INSERT INTO orderpercustomer (idorder, idcustomer)" + " VALUES (?,?)";
	private static final String insertStatementString2 = "INSERT INTO orderproduct (idorder, quantity, products_idproducts)" + " VALUES (?,?,?)";
	private final static String findStatementString1 = "SELECT c.idcustomer, c.name, c.address, c.phoneNumber, p.idproducts, p.name, p.price, op.idorder, op.quantity FROM customer c, orderpercustomer oc, orderproduct op, products p WHERE op.products_idproducts = p.idproducts AND c.idcustomer = oc.idcustomer AND oc.idorder = ?";
	private final static String findStatementString2 = "SELECT c.idcustomer, c.name, c.address, c.phoneNumber, p.idproducts, p.name, p.price, op.idorder, op.quantity FROM customer c, orderpercustomer oc, orderproduct op, products p WHERE op.products_idproducts = p.idproducts AND c.idcustomer = oc.idcustomer AND oc.idorder = op.idorder AND c.name = ?";
	private final static String findStatementString3 = "SELECT c.idcustomer, c.name, c.address, c.phoneNumber, p.idproducts, p.name, p.price, p.inStock, op.idorder, op.quantity FROM customer c, orderpercustomer oc, orderproduct op, products p WHERE op.products_idproducts = p.idproducts AND c.idcustomer = oc.idcustomer AND oc.idorder = op.idorder";
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then returns the order found by id
	 */
	
	//@SuppressWarnings("null")
	public static Order findById(int orderid) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			int totalPrice = 0;
			findStatement = dbConnection.prepareStatement(findStatementString1);
			findStatement.setLong(1, orderid);
			rs = findStatement.executeQuery();
			rs.next();
			Customer c = new Customer (rs.getInt("c.idcustomer"), rs.getString("c.name"), rs.getString("c.address"), rs.getInt("phoneNumber"), false);
			ArrayList <Products> p = new ArrayList<Products>();
			p.add(new Products(rs.getInt("p.idproducts"), rs.getString("p.name"), rs.getInt("p.price"), rs.getInt("op.quantity")));
			totalPrice += rs.getInt("p.price") * rs.getInt("op.quantity");
			
			while (rs.next()) {
				p.add(new Products(rs.getInt("p.idproducts"), rs.getString("p.name"), rs.getInt("p.price"), rs.getInt("op.quantity")));
				totalPrice += rs.getInt("p.price") * rs.getInt("op.quantity");
			}
			
			toReturn = new Order(orderid, c, p, totalPrice);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then returns the order found by customer's name
	 */
	
	public static Order findByName(String name) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			int totalPrice = 0;
			int id;
			findStatement = dbConnection.prepareStatement(findStatementString2);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();
			id = rs.getInt("op.idorder");
			Customer c = new Customer (rs.getInt("c.idcustomer"), name, rs.getString("c.address"), rs.getInt("phoneNumber"), false);
			ArrayList <Products> p = new ArrayList<Products>();
			p.add(new Products(rs.getInt("p.idproducts"), rs.getString("p.name"), rs.getInt("p.price"), rs.getInt("op.quantity")));
			totalPrice += rs.getInt("p.price") * rs.getInt("op.quantity");
			
			while (rs.next()) {
				p.add(new Products(rs.getInt("p.idproducts"), rs.getString("p.name"), rs.getInt("p.price"), rs.getInt("op.quantity")));
				totalPrice += rs.getInt("p.price") * rs.getInt("op.quantity");
			}
			
			toReturn = new Order(id, c, p, totalPrice);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findByName" + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then returns the found by orders
	 */
	
	public static ArrayList<Order> displayOrder() {
		ArrayList<Order> toReturn = new ArrayList<Order>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			int totalPrice;
			findStatement = dbConnection.prepareStatement(findStatementString3);
			rs = findStatement.executeQuery();
			int idorder;
			rs.last();
			int numOfCol = rs.getRow();
			rs.beforeFirst();
			rs.next();
			while (numOfCol != 0) {
				totalPrice = 0;
				Customer c = new Customer (rs.getInt("c.idcustomer"), rs.getString("c.name"), rs.getString("c.address"), rs.getInt("phoneNumber"), false);
				ArrayList <Products> p = new ArrayList<Products>();
				p.add(new Products(rs.getInt("p.idproducts"), rs.getString("p.name"), rs.getInt("p.price"), rs.getInt("op.quantity"), rs.getInt("op.quantity")));
				totalPrice += rs.getInt("p.price") * rs.getInt("op.quantity");
				idorder = rs.getInt("op.idorder");
				
				while (rs.next() && rs.getInt("op.idorder") == idorder) {
					p.add(new Products(rs.getInt("p.idproducts"), rs.getString("p.name"), rs.getInt("p.price"), rs.getInt("op.quantity"), rs.getInt("p.inStock")));
					totalPrice += rs.getInt("p.price") * rs.getInt("op.quantity");
					numOfCol--;
				}
				numOfCol--;
				toReturn.add(new Order(idorder, c, p, totalPrice));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:displayOrder " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	/*
	 * Instantiates the connection to the database, prepare and execute the query, then inserts the order
	 */

	public static int insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString1, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getOrderid());
			insertStatement.setInt(2, order.getCustomer().getCustomerid());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
			
			int l = order.getProducts().size();
			for (int i = 0; i < l; i++) {
				insertStatement = dbConnection.prepareStatement(insertStatementString2, Statement.RETURN_GENERATED_KEYS);
				insertStatement.setInt(1, order.getOrderid());
				insertStatement.setInt(2, order.getProducts().get(i).getQuantity());
				insertStatement.setInt(3, order.getProducts().get(i).getProductid());
				insertStatement.executeUpdate();
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDao:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}
