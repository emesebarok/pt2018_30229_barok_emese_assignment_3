package frame;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * View which displays two buttons for the admin: Modify Warehouse and Display Orders.
 * @author Barok Emese
 *
 */

public class AdminMenuFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btnModifyWarehouse;
	private JButton btnDisplayOrders;
	
	/**
	 * Creates a frame with predefined options for size, defaultCloseOperation etc.
	 * The user interface is also created step by step using Java Swing classes.
	 */
	public AdminMenuFrame() {
		this.setTitle("Admin Menu");
		this.setSize(350, 250);
		this.setLayout(new GridLayout(0,1));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.setLayout(new GridLayout(2,1));
		
		btnModifyWarehouse = new JButton("Modify Warehouse");
		btnDisplayOrders = new JButton("Display Orders");
		this.add(btnModifyWarehouse);
		this.add(btnDisplayOrders);
		
		this.setVisible(true);
	}
	
	public JButton getBtnModifyWarehouse() {
		return btnModifyWarehouse;
	}
	
	public JButton getBtnDisplayOrders() {
		return btnDisplayOrders;
	}

}
