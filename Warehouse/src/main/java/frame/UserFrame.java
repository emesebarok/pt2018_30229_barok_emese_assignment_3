package frame;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class UserFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btnModifyData;
	private JButton btnPlaceOrders;
	
	/**
	 * Creates a frame with predefined options for size, defaultCloseOperation etc.
	 * The user interface is also created step by step using Java Swing classes.
	 */
	public UserFrame() {
		this.setTitle("User Menu");
		this.setSize(350, 250);
		this.setLayout(new GridLayout(0,1));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.setLayout(new GridLayout(2,1));
		
		btnModifyData = new JButton("Modify Data");
		btnPlaceOrders = new JButton("Place Orders");
		this.add(btnModifyData);
		this.add(btnPlaceOrders);
		
		this.setVisible(true);
	}
	
	public JButton getBtnModifyData() {
		return btnModifyData;
	}



	public JButton getBtnPlaceOrders() {
		return btnPlaceOrders;
	}

}
